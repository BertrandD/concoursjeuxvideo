#include "Button.hpp"

Button::Button(int x, int y, Menu *menu, sf::Texture *texture, sf::Texture *text) : _active(false), _i(x), _j(y) {
    _texture = texture;
    _textureText = text;
    _menu = menu;
}

void Button::draw() {
    _sprite.setTexture(*_texture);
    if (_active) {
        _sprite.setTextureRect(sf::IntRect(256, 0, 256, 64));
    } else {
        _sprite.setTextureRect(sf::IntRect(0, 0, 256, 64));
    }
    _sprite.setPosition(_i, _j);
    _menu->getWindow()->draw(_sprite);
    _sprite.setTexture(*_textureText);
    _sprite.setTextureRect(sf::IntRect(0, 0, 256, 64));
    _menu->getWindow()->draw(_sprite);
}

void Button::setActive(bool active) {
    _active = active;
}
