#ifndef BUTTON_H
#define BUTTON_H

#include "../Engine/Menu.hpp"
#include <SFML/Graphics.hpp>

class Button {
public:
    Button(int x, int y, Menu *menu, sf::Texture *texture, sf::Texture *text);

    void draw();

    void setActive(bool active);

protected:
private:
    bool _active;
    int _i, _j;
    Menu *_menu;
    sf::Texture *_texture;
    sf::Texture *_textureText;
    sf::Sprite _sprite;

};

#endif // BUTTON_H
