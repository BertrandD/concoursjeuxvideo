
#include "Engine/Menu.hpp"

int main() {
    //Dimensions multiples de 32 ! (800x640 px <-> 25x20 cases)
    Menu menu(800, 640);
    menu.run();

    return 0;
}
