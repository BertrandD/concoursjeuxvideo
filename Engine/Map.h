//
// Created by bertrand on 16/05/17.
//

#ifndef CONCOURSJEUXVIDEO_MAP_H
#define CONCOURSJEUXVIDEO_MAP_H

#include "../Objects/Object.hpp"
#include "../Objects/EndMap.hpp"
#include "../Objects/Sensor.hpp"
#include "SFML/Graphics.hpp"

class Map {
public:
    static void loadMap(std::string *mapPath);

    static Map *getMap();

    virtual ~Map();

    void checkSensors();

    bool checkNextMap();

    static Direction getDirection(char text);

    static sf::Color getColor(char text);

    Object *_objects[20][25];
    Sensor **_sensors;
    EndMap *_endMaps[2];
    int _nbSensors;
    int _nbWalls;
    int player1InitialX;
    int player1InitialY;
    int player2InitialX;
    int player2InitialY;
private:
    static Map *_map;
};


#endif //CONCOURSJEUXVIDEO_MAP_H
