//
// Created by bertrand on 16/05/17.
//

#include "Map.h"
#include "../Objects/Wall.hpp"
#include "../Objects/Lazer.hpp"
#include "../Objects/Mirror.hpp"
#include "../Objects/Window.hpp"
#include "../Objects/Portal.hpp"
#include "../Objects/Filter.hpp"
#include "../Objects/Concentrator.hpp"

Map *Map::_map = nullptr;


void Map::loadMap(std::string *mapPath)
/*
	@author : Bertrand D.
	Cette méthode permet de charger la map écrite dans le fichier .map
	dans un tableau bidimensionnel dont ont renvoie l'adresse
	NB : Il est très important de respecter précisément la syntaxe des fichiers .map
		établie au dessus sinon les objets ne seront pas placés correctement.
*/
{
    Map *map = new Map();
#pragma GCC diagnostic ignored "-Wuninitialized" //Supprime un warning sur le switch
    char *temp = (char *) malloc(3 * sizeof(char));
    map->_sensors = (Sensor **) malloc(
            10 * sizeof(Sensor *)); // 10 Sensor max par map... suffisant ?? @Ph: Trop même.
    Sensor *sensorTemp;
    int n;
    map->_nbSensors = 0;
    FILE *f;
    f = fopen((*mapPath).c_str(), "r");
    if (f == NULL) perror("Error opening map");
    else {
        sf::Color color;
        Direction dir;
        for (int j = 0; j < 20; ++j) {
            for (int i = 0; i < 25; ++i) {
                n = fscanf(f, "%s ", temp);
                if (n != EOF) {
                    switch (temp[0]) {
                        case '1': //Wall
                            map->_objects[j][i] = (Object *) new Wall(i, j);
                            break;
                        case '-': //End
                            if (temp[1] == '1') {
                                if (temp[3] == 'B') {
                                    map->_objects[j][i] = (Object *) new EndMap(i, j, "Resources/Textures/Door_B.png",
                                                                                "Resources/Textures/Door_Open_B.png");
                                    map->_endMaps[1] = (EndMap *) map->_objects[j][i];
                                } else {
                                    map->_objects[j][i] = (Object *) new EndMap(i, j, "Resources/Textures/Door_T.png",
                                                                                "Resources/Textures/Door_Open_T.png");
                                    map->_endMaps[0] = (EndMap *) map->_objects[j][i];
                                }
                            } else {
                                map->_objects[j][i] = (Object *) new Object(i, j);
                            }
                            break;
                        case '2': //lazer
                            color = getColor(temp[2]);
                            dir = getDirection(temp[4]);
                            map->_objects[j][i] = (Object *) new Lazer(i, j, color, dir);
                            break;
                        case '3': // capteur
                            color = getColor(temp[2]);
                            dir = getDirection(temp[4]);
                            sensorTemp = new Sensor(i, j, color, dir);
                            map->_objects[j][i] = (Object *) sensorTemp;
                            map->_sensors[map->_nbSensors] = sensorTemp;
                            map->_nbSensors++;
                            break;
                        case '4': // Mirroir test
                            dir = getDirection(temp[2]);
                            map->_objects[j][i] = (Object *) new Mirror(i, j, dir);
                            break;
                        case '5': // Fenêtre
                            dir = getDirection(temp[2]);
                            map->_objects[j][i] = (Object *) new Window(i, j, dir);
                            break;
                        case '6': // Portail
                            map->_objects[j][i] = (Object *) new Portal(i, j, atoi(&temp[2]));
                            break;
                        case '7': // Concentrator - Check du fonctionnement requis !!
                            dir = getDirection(temp[2]);
                            map->_objects[j][i] = (Concentrator *) new Concentrator(i, j, dir);
                            break;
                        case '8': // Filter
                            color = getColor(temp[2]);
                            map->_objects[j][i] = (Filter *)
                                    new Filter(i, j, color);
                            break;
                        default:
                            map->_objects[j][i] = (Object *) new Object(i, j);
                            break;
                    }
                }
            }
        }
    }
    fscanf(f, "Player1:%s ", temp);
    int x = atoi(temp);
    fscanf(f, "%s ", temp);
    int y = atoi(temp);
    map->player1InitialX = x;
    map->player1InitialY = y;

    fscanf(f, "Player2:%s ", temp);
    x = atoi(temp);
    fscanf(f, "%s ", temp);
    y = atoi(temp);
    map->player2InitialX = x;
    map->player2InitialY = y;

    map->_sensors = (Sensor **) realloc(map->_sensors, map->_nbSensors * sizeof(Sensor *));
    free(temp);
    fclose(f);
    if (_map != nullptr) {
        delete _map;
    }
    _map = map;
}

void Map::checkSensors() {
    bool allSensorsActivated = true;
    for (int i = 0; i < _nbSensors; ++i) {
        if (!_sensors[i]->isActive())
            allSensorsActivated = false;
        _sensors[i]->disable();
    }
    if (allSensorsActivated) {
        _endMaps[0]->open();
        _endMaps[1]->open();
    } else {
        _endMaps[0]->close();
        _endMaps[1]->close();
    }
}

Direction Map::getDirection(char text) {
    Direction dir;
    switch (text) // Direction
    {
        case 'N':
            dir = Direction::N;
            break;
        case 'E':
            dir = Direction::E;
            break;
        case 'S':
            dir = Direction::S;
            break;
        case 'W':
            dir = Direction::W;
            break;
        default:
            dir = Direction::N;
            break;
    }
    return dir;
}

sf::Color Map::getColor(char text) {
    sf::Color color;
    switch (text) // couleur
    {
        case '1':
            color = sf::Color::Blue;
            break;
        case '2' :
            color = sf::Color::Green;
            break;
        case '3':
            color = sf::Color::Cyan;
            break;
        case '4':
            color = sf::Color::Red;
            break;
        case '5':
            color = sf::Color::Yellow;
            break;
        case '6':
            color = sf::Color::Magenta;
            break;
        case '7':
            color = sf::Color::Black;
            break;
        default:
            color = sf::Color::White;
            break;
    }
    return color;
}

Map *Map::getMap() {
    return _map;
}

bool Map::checkNextMap() {
    return _endMaps[0]->isReached() || _endMaps[1]->isReached();
}

Map::~Map() {
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 25; ++j) {
            delete (this->_objects[i][j]);
        }
    }
    float nbSensors = sizeof(this->_sensors) / sizeof(Sensor);
    for (int i = 0; i < nbSensors; ++i) {
        delete (&this->_sensors[i]);
    }
}
