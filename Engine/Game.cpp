#include "Game.hpp"
#include <iostream>


using namespace std;

//On construit la classe en appliquant un titre à la fenêtre et en lui donnant une taille de 640x480px
//Puis on définit les propriétées du joueur
Game::Game(int width, int height) :
        _Window(sf::VideoMode(width, height), "The 418 Game"),
        _player1(new Player("Player 1", sf::Color::Blue, 3, 3)),
        _player2(new Player("Player 2", sf::Color::Green, 3, 3, sf::Keyboard::Up, sf::Keyboard::Left,
                            sf::Keyboard::Down, sf::Keyboard::Right)),
        _nbTry(0),
        _font() {
    cout << "#################################" << endl;
    cout << "#\tConcours Jeu Vidéo\t#" << endl;
    cout << "#\t\t2015\t\t#" << endl;
    cout << "#################################" << endl;
    cout << "" << endl;
    cout << "Participants :" << endl;
    cout << "\tBertrand Darbon" << endl;
    cout << "\tAlexandre Delolme" << endl;
    cout << "\tPhilippe Leboc" << endl;
    cout << "\tAlexandre Muhl" << endl;

    cout << "Lancement du jeu avec deux joueurs." << endl;

    /* @author : Bertrand D
        Initialisation de la matrice de jeu.
        false = aucun objet présent, le joueur peut y aller
        true  = objet présent, le joueur ne peut pas y aller
    */
    cout << "[ ok ] Initialisation de la matrice de jeu" << endl;

    cout << "[ ... ] Chargement des textures";

    cout << "\r[ ok ] Chargement des textures  " << endl;

    cout << "[ ... ] Chargement de la map";
    /*
        @author : Bertrand D.
        @edit	: Philippe L.
        Dans matrix est chargé la map écrite dans les fichiers .map
        Il faut bien respecter la sytaxe de ces fichiers comme le fichier exemple map1.map
        -> 20 lignes et 25 colonnes
        -> Chaque valeur (entière!) espacée d'un seul espace
        Les valeurs suivantes sont actuellement prises en compte :

        ->-2 : Enter Map
        ->-1 : Exit Map
        -> 0 : Void Object
        -> 1 : Wall
        -> 2 : Laser
        -> 3 : Sensor
        -> 4 : Mirror
        -> 5 : Window
        -> 6 : Portal
        -> 7 : Concentrator
        -> 8 : Filter

        Certains objets acceptent des couleurs, ainsi (2:2) donnera un Laser Vert:
        -> 1 : Bleu
        -> 2 : Vert
        -> 3 : Cyan
        -> 4 : Red
        -> ...

        Et enfin pour certains nous auront une direction:
        -> N : Nord
        -> S : South
        -> W : West
        -> E : Est
        -> UNDIFINED : c'est assez clair non ?

        Cette gestion des valeurs est actuellement un peu archaïque mais sera améliorée par la suite
        @Philippe L: On a pas le temps !
    */
    _maps[0] = "Map/map1.map";
    _maps[1] = "Map/map2.map";
    _maps[2] = "Map/map3.map";
    _maps[3] = "Map/map4.map";

    Map::loadMap(&_maps[0]);
    _player1->setPosition(Map::getMap()->player1InitialX, Map::getMap()->player1InitialY);
    _player2->setPosition(Map::getMap()->player2InitialX, Map::getMap()->player2InitialY);
    cout << "\r[ ok ] Chargement de la map n°1" << "/" << sizeof(_maps) / sizeof(_maps[0]) << endl;

    cout << "[ ... ] Chargement polices caractère";
    if (!_font.loadFromFile("Resources/arial.ttf"))
        cout << "Erreur à l'ouverture du fichier font arial.ttf" << endl;
    cout << "\r[ ok ] Chargement polices caractère  " << endl;

    cout << "[ ... ] Chargement musique";
    if (!_music.openFromFile("Resources/Music/TicTacOrchestra.ogg"))
        cout << "Erreur chargement fichier musique";
    _music.setLoop(true);
    cout << "\r[ ok ] Chargement musique " << endl;

    if (!_inputsTexture.loadFromFile("Resources/Menu/Inputs.png"))
        cout << "Erreur chargement Inputs.png" << endl;

    if (!_backgroundText.loadFromFile("Resources/Menu/Background.png"))
        cout << "Erreur chargement Background.png" << endl;
    _background.setTexture(_backgroundText);
}

void Game::nextMap(bool reload) {
    static int numMap = 1;
    if (reload) {
        _nbTry++;
        numMap--;
    } else {
        _nbTry = 0;
    }
    float nbMaps = sizeof(_maps) / sizeof(_maps[0]);
    if (numMap < nbMaps) {
        Map::loadMap(&_maps[numMap]);
        _player1->setPosition(Map::getMap()->player1InitialX, Map::getMap()->player1InitialY);
        _player2->setPosition(Map::getMap()->player2InitialX, Map::getMap()->player2InitialY);
        numMap++;
        cout << endl << "[ ok ] Chargement de la map n°" << numMap << "/" << nbMaps << "    " << endl;
    } else {
        numMap = 1;
        _Window.close();
    }
}

void Game::run() {

    sf::Clock clock;
    //Déclaration d'une variable comptant le temps écoulé entre chaques frames
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
    _music.play();
    while (_Window.isOpen()) {
        processEvents();
        //Cette fois, on ajoute le temps de la derniere frame sans réinitialisation
        timeSinceLastUpdate += clock.restart();
        //On attend qu'il se soit écoulé au moins la durée d'une frame fixe
        while (timeSinceLastUpdate > TimePerFrame) {
            //On soustrait le temps d'une frame fixe au temps qui s'est réélement écoulé
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);
            _player1->update(TimePerFrame);
            _player2->update(TimePerFrame);

//            cout << "\r Player1 (x:" << _player1->getPosition().x << " y:" << _player1->getPosition().y
//                 << ") - Player 2 (x:" << _player2->getPosition().x << " y:" << _player2->getPosition().y
//                 << ")           ";

            // cout << endl;
            // for (int j = 0; j < 20; ++j)
            // {
            // 	for (int i = 0; i < 25; ++i)
            // 	{
            // 		cout << _objects[j][i]->isActive() << " ";
            // 	}
            // 	cout << endl;
            // }
            // cout << endl;
        }
        render();
        Map::getMap()->checkSensors();
        if (Map::getMap()->checkNextMap()) {
            nextMap(false);
        }
    }
    cout << endl;
}

void Game::processEvents() {
    sf::Event event;
    while (_Window.pollEvent(event)) {
#pragma GCC diagnostic ignored "-Wswitch" //Supprime un warning sur le switch
        switch (event.type) {
            case sf::Event::KeyPressed:
                // if(event.key.code == sf::Keyboard::E)
                // 	nextMap(false);
                if (event.key.code == sf::Keyboard::R)
                    nextMap(true);
                if (event.key.code == sf::Keyboard::LControl)
                    _player1->toggleWalk();
                if (event.key.code == sf::Keyboard::RControl)
                    _player2->toggleWalk();
                _player1->handlePlayerInput(event.key.code, true);
                _player2->handlePlayerInput(event.key.code, true);
                break;
            case sf::Event::KeyReleased:
                _player1->handlePlayerInput(event.key.code, false);
                _player2->handlePlayerInput(event.key.code, false);
                break;
            case sf::Event::Closed:
                _Window.close();
                break;
            default:
                break;
        }
    }
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void Game::update(sf::Time deltaTime) {}

void Game::drawHUD() {
    sf::Text text;
    text.setFont(_font);
//	text.setString("Nombre d'essais : "+std::to_string(_nbTry));
    text.setCharacterSize(24);
    text.setColor(sf::Color::White);
    _Window.draw(text);

}

void Game::render() {
    _Window.clear(sf::Color::White); //Efface le contenu de l'écran
    _Window.draw(_background);
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 25; ++j) {
            Map::getMap()->_objects[i][j]->drawObject(&_Window);
        }
    }
    for (int i = 0; i < 20; ++i) {
        for (int j = 0; j < 25; ++j) {
            Map::getMap()->_objects[i][j]->resetInitial();
        }
    }
    _Window.draw(_player1->_sprite);
    _Window.draw(_player2->_sprite);
    drawHUD();
    _Window.display(); //Affiche l'écran.
}

Game::~Game() {
    delete (_player1);
    delete (_player2);
}