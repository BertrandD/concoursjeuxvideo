#ifndef MENU_HPP
#define MENU_HPP

#include <SFML/Graphics.hpp>
#include "SFML/Audio.hpp"
#include <string>

class Menu;

#include "../Menu/Button.hpp"

class Button;

class Menu {
public:
    Menu(int width, int height);

    ~Menu();

    void run(); //Permet de lancer le jeu
    sf::RenderWindow *getWindow();

private:
    sf::RenderWindow _Window; //Le jeu gère sa propre fenêtre
    sf::Font _font;
    Button *_button1;
    Button *_button2;
    Button *_button3;
    sf::Texture _buttonTexture;
    sf::Texture _launchTexture;
    sf::Texture _inputsTexture;
    sf::Texture _creditsTexture;
    sf::Texture _404Present;
    sf::Texture _background;
    sf::Texture _inputs;
    sf::Texture _credits;
    sf::Texture _logo;
    sf::Music _music;
    int menuIndex;

    void render();

    void update(sf::Time deltaTime);

    void processEvents();

    void initTextures(std::string path);

    void action(sf::Keyboard::Key key);

    bool beforeMenu;
    bool inputs;
    bool credits;
};


#endif
