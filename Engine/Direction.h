//
// Created by bertrand on 16/05/17.
//

#ifndef CONCOURSJEUXVIDEO_DIRECTION_H
#define CONCOURSJEUXVIDEO_DIRECTION_H

enum Direction {
    UNDEFINED,
    N,
    S,
    W,
    E
};

#endif //CONCOURSJEUXVIDEO_DIRECTION_H
