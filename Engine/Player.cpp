#include "Player.hpp"
#include "Map.h"

Player::Player(std::string name, sf::Color color, unsigned int i, unsigned int j) :
        _sprite(),
        _name(name),
        _color(color),
        _speed(150.0),
        _angle(90),
        _anim(0),
        _sheet() {
    initSprite();
    initKeyboard();
    float x = 32 * i + 16;
    float y = 32 * j + 32;
    _sprite.setPosition(x, y);
}

Player::Player(std::string name, sf::Color color, unsigned int i, unsigned int j, sf::Keyboard::Key up,
               sf::Keyboard::Key left, sf::Keyboard::Key down, sf::Keyboard::Key right) :
        _sprite(),
        _name(name),
        _color(color),
        _speed(150.0),
        _up(up),
        _left(left),
        _down(down),
        _right(right),
        _angle(90),
        _anim(0),
        _sheet() {
    initSprite();
    float x = 32 * i + 16;
    float y = 32 * j + 32;
    _sprite.setPosition(x, y);
}

void Player::initSprite() {
    if (!_texture.loadFromFile("Resources/Textures/Player.png")) {
        // erreur...
    }
    _sprite.setTexture(_texture);
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 6; j++) {
            _sprite.setTextureRect(sf::IntRect(j * 32, i * 32, 32, 32));
            _sheet.push_back(_sprite);
        }
    }
    _sprite.setOrigin(16, 16);
    _sprite.setColor(_color);
    updateSprite();
}

// Associe à _sprite l'image correspondant à son animation en fonction de _angle et de _anim
void Player::updateSprite() {
    int pos = (_anim / 10) % 6;
    /*if((int)_angle%2)
        pos+=6;*/
    sf::Vector2f p = _sprite.getPosition();
    float r = _sprite.getRotation();
    _sprite = _sheet.at(pos);
    _sprite.setPosition(p);
    _sprite.setColor(_color);
    _sprite.setRotation(r);
    _sprite.setOrigin(16, 16);
    _sprite.setScale(0.9, 0.9);
}

void Player::initKeyboard() {
    _up = sf::Keyboard::Z;
    _down = sf::Keyboard::S;
    _left = sf::Keyboard::Q;
    _right = sf::Keyboard::D;
}

void Player::toggleWalk() {
    if (_speed == 150.0) {
        _speed = 50;
    } else {
        _speed = 150;
    }
}

sf::Vector2f Player::getPosition() {
    return _sprite.getPosition();
}

void Player::setPosition(int i, int j) {
    _sprite.setPosition(i * 32 + 16, j * 32 + 16);
}

void Player::handlePlayerInput(sf::Keyboard::Key key, bool isPressed) {
    if (key == _up)
        _IsMovingUp = isPressed;
    else if (key == _down)
        _IsMovingDown = isPressed;
    else if (key == _left)
        _IsMovingLeft = isPressed;
    else if (key == _right)
        _IsMovingRight = isPressed;
}

void Player::update(sf::Time deltaTime) {
    sf::Vector2f movement(0.f, 0.f);
    if (_IsMovingUp)
        movement.y -= 1.f;
    if (_IsMovingDown)
        movement.y += 1.f;
    if (_IsMovingLeft)
        movement.x -= 1.f;
    if (_IsMovingRight)
        movement.x += 1.f;
    movement.x *= deltaTime.asSeconds() * _speed;
    movement.y *= deltaTime.asSeconds() * _speed;

    float beta;
    float gamma = _angle;
    if (movement.x == 0 && movement.y == 0)
        beta = _angle;
    else if (movement.x > 0 && movement.y == 0)
        beta = 0;
    else if (movement.x < 0 && movement.y == 0)
        beta = 180;
    else if (movement.x == 0 && movement.y > 0)
        beta = 270;
    else if (movement.x > 0 && movement.y > 0)
        beta = 315;
    else if (movement.x < 0 && movement.y > 0)
        beta = 225;
    else if (movement.x == 0 && movement.y < 0)
        beta = 90;
    else if (movement.x > 0 && movement.y < 0)
        beta = 45;
    else
        beta = 135;
    if (_angle != beta) {
        if (_angle < beta) {
            if (beta - _angle > 180) {
                _angle -= 45;
                _sprite.rotate(45);
            } else {
                _angle += 45;
                _sprite.rotate(-45);
            }
        } else {
            if (_angle - beta > 180) {
                _angle += 45;
                _sprite.rotate(-45);
            } else {
                _angle -= 45;
                _sprite.rotate(45);
            }
        }
    }
    _angle = (int) _angle % 360;
    if (_angle < 0)
        _angle += 360;
    if (_angle != gamma)
        _anim += (10 - _anim % 10);
    if (_anim % 10 == 0)
        updateSprite();
    if (!(movement.x == 0 && movement.y == 0))
        _anim++;
    _anim %= 60;
    sendActions(&movement);
    advancedMove(&movement);
    _sprite.move(movement);

}

void Player::sendActions(const sf::Vector2f *movement) {
    //Action() sur la case où est le joueur
    int i = (_sprite.getPosition().x) / 32;
    int j = (_sprite.getPosition().y) / 32;
    Map::getMap()->_objects[j][i]->action(this);


    int posX = (int) _sprite.getPosition().x - 16 + movement->x;
    int posY = (int) _sprite.getPosition().y - 16 + movement->y;

    int padding = 4;

    if (movement->x > 0) {
        //en haut à droite
        i = (posX + 32 - padding) / 32;
        j = (posY + padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(E));
        //en bas à droite
        i = (posX + 32 - padding) / 32;
        j = (posY + 32 - padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(E));
    } else if (movement->x < 0) {
        //en haut à gauche
        i = (posX + padding) / 32;
        j = (posY + padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(W));
        //en bas à gauche
        i = (posX + padding) / 32;
        j = (posY + 32 - padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(W));
    }
    if (movement->y > 0) {
        //en bas à gauche
        i = (posX + padding) / 32;
        j = (posY + 32 - padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(S));
        //en bas à droite
        i = (posX + 32 - padding) / 32;
        j = (posY + 32 - padding) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(S));
    } else if (movement->y < 0) {
        //en haut à gauche
        i = (posX + padding) / 32;
        j = (posY) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(N));
        //en haut à droite
        i = (posX + 32 - padding) / 32;
        j = (posY) / 32;
        Map::getMap()->_objects[j][i]->action(Direction(N));
    }
}


void Player::advancedMove(sf::Vector2f *movement) {
    float temp;

    temp = movement->x;
    movement->x = 0;
    if (!canMove(movement))
        movement->y = 0;
    movement->x = temp;

    temp = movement->y;
    movement->y = 0;
    if (!canMove(movement))
        movement->x = 0;
    movement->y = temp;
}

/*
	Fonction Principale de la HitBox permettant de déterminer
	(avant déplacement du personnage) si celui-ci va rencontrer
	une collision ou si rien n'est sur son passage.
*/
bool Player::canMove(sf::Vector2f *movement) {
    bool canMove = true;

    // position des pieds du personnages [<-32->] donc le milieu (32/2) = 16
    // Pour le Y, rien de plus simple, Position.y + 32 vu qu'à la base ca
    // retourne le coin supérieure gauche
    // On ajoute movement.x et movement.y car on veut prédire la destination.
    float posX = _sprite.getPosition().x - 16 + movement->x;
    float posY = _sprite.getPosition().y - 16 + movement->y;

    int padding = 5;
    //en haut à gauche
    int i = (posX + padding) / 32;
    int j = (posY + padding) / 32;
    canMove = canMove && !(Map::getMap()->_objects[j][i]->isSolid()) && posX >= 0 && posY >= 0 && posX <= 800 &&
              posY <= 640;

    //en haut à droite
    i = (posX + 32 - padding) / 32;
    j = (posY + padding) / 32;
    canMove = canMove && !(Map::getMap()->_objects[j][i]->isSolid()) && posX >= 0 && posY >= 0 && posX <= 800 &&
              posY <= 640;

    //en bas à gauche
    i = (posX + padding) / 32;
    j = (posY + 32 - padding) / 32;
    canMove = canMove && !(Map::getMap()->_objects[j][i]->isSolid()) && posX >= 0 && posY >= 0 && posX <= 800 &&
              posY <= 640;

    //en bas à droite
    i = (posX + 32 - padding) / 32;
    j = (posY + 32 - padding) / 32;
    canMove = canMove && !(Map::getMap()->_objects[j][i]->isSolid()) && posX >= 0 && posY >= 0 && posX <= 800 &&
              posY <= 640;

    return canMove;
}
