#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "SFML/Graphics.hpp"

class Player;

#include <string>

class Player : sf::Transformable {
public:
    Player(std::string name, sf::Color color, unsigned int i, unsigned int j);

    Player(std::string name, sf::Color color, unsigned int i, unsigned int j, sf::Keyboard::Key up,
           sf::Keyboard::Key left, sf::Keyboard::Key down, sf::Keyboard::Key right);

    void handlePlayerInput(sf::Keyboard::Key key, bool isPressed);

    void update(sf::Time deltaTime);

    sf::Sprite _sprite;

    sf::Vector2f getPosition();

    void setPosition(int i, int j);

    void toggleWalk();

private:
    void initSprite();

    void initKeyboard();

    void advancedMove(sf::Vector2f *movement); // hitbox
    bool canMove(sf::Vector2f *movement); // hitbox
    void sendActions(const sf::Vector2f *movement);

    void updateSprite();

    std::string _name;
    sf::Texture _texture;
    sf::Color _color;
    float _speed;
    bool _IsMovingUp = false;
    bool _IsMovingDown = false;
    bool _IsMovingLeft = false;
    bool _IsMovingRight = false;
    sf::Keyboard::Key _up;
    sf::Keyboard::Key _left;
    sf::Keyboard::Key _down;
    sf::Keyboard::Key _right;
    float _angle;
    int _anim;
    std::vector<sf::Sprite> _sheet;
    float _padding;
};

#endif
