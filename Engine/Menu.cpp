#include "Menu.hpp"
#include "Game.hpp"
#include <iostream>

using namespace std;

Menu::Menu(int width, int height) :
        _Window(sf::VideoMode(width, height), "The 418 Game : Menu") {
    initTextures("Resources/Menu/French/");
    _button1 = (Button *) new Button(272, 300, this, &_buttonTexture, &_launchTexture);
    _button2 = (Button *) new Button(272, 400, this, &_buttonTexture, &_inputsTexture);
    _button3 = (Button *) new Button(272, 500, this, &_buttonTexture, &_creditsTexture);
    menuIndex = 1;
    beforeMenu = true;
    inputs = false;
    credits = false;
    if (!_music.openFromFile("Resources/Music/SuperPeanut.ogg")) {
        cout << "Erreur lors du chargement de SuperPeanut.ogg" << endl;
    }
    _music.setLoop(true);
}

void Menu::run() {
    _music.play();
    sf::Clock clock;
    //Déclaration d'une variable comptant le temps écoulé entre chaques frames
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    sf::Time TimePerFrame = sf::seconds(1.f / 60.f);
    while (_Window.isOpen()) {
        processEvents();
        //Cette fois, on ajoute le temps de la derniere frame sans réinitialisation
        timeSinceLastUpdate += clock.restart();
        //On attend qu'il se soit écoulé au moins la durée d'une frame fixe
        while (timeSinceLastUpdate > TimePerFrame) {
            //On soustrait le temps d'une frame fixe au temps qui s'est réélement écoulé
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();
            update(TimePerFrame);
        }
        render();
    }
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void Menu::update(sf::Time deltaTime) {
    _button1->setActive(false);
    _button2->setActive(false);
    _button3->setActive(false);
    switch (menuIndex) {
        case 1:
            _button1->setActive(true);
            break;
        case 2:
            _button2->setActive(true);
            break;
        case 3:
            _button3->setActive(true);
            break;
    }
}

void Menu::processEvents() {
    sf::Event event;
    while (_Window.pollEvent(event)) {
#pragma GCC diagnostic ignored "-Wswitch" //Supprime un warning sur le switch
        switch (event.type) {
            case sf::Event::Closed:
                _Window.close();
                break;
            case sf::Event::KeyPressed:
                action(event.key.code);
            default:
                break;
        }
    }
}

void Menu::render() {
    _Window.clear(sf::Color::White); //Efface le contenu de l'écran
    if (beforeMenu) {
        sf::Sprite sprite;
        sprite.setTexture(_404Present);
        sprite.setPosition(0, 0);
        _Window.draw(sprite);
    } else if (inputs) {
        sf::Sprite sprite;
        sprite.setTexture(_inputs);
        sprite.setPosition(0, 0);
        _Window.draw(sprite);
    } else if (credits) {
        sf::Sprite sprite;
        sprite.setTexture(_credits);
        sprite.setPosition(0, 0);
        _Window.draw(sprite);
    } else {
        sf::Sprite sprite;
        sprite.setTexture(_background);
        sprite.setPosition(0, 0);
        _Window.draw(sprite);
        sprite.setTexture(_logo);
        sprite.setPosition(25, 25);
        _Window.draw(sprite);
        _button1->draw();
        _button2->draw();
        _button3->draw();
    }
    _Window.display(); //Affiche l'écran.
}

void Menu::initTextures(std::string path) {
    if (!_buttonTexture.loadFromFile(path + "button.png")) {
        cout << "Erreur chargement button.png" << endl;
    }
    if (!_launchTexture.loadFromFile(path + "Launch.png")) {
        cout << "Erreur chargement Launch.png" << endl;
    }
    if (!_inputsTexture.loadFromFile(path + "Inputs.png")) {
        cout << "Erreur chargement Inputs.png" << endl;
    }
    if (!_creditsTexture.loadFromFile(path + "Credits.png")) {
        cout << "Erreur chargement Credits.png" << endl;
    }
    if (!_404Present.loadFromFile("Resources/Menu/404NotFoundPresent.png")) {
        cout << "Erreur chargement 404NotFoundPresent.png" << endl;
    }
    if (!_background.loadFromFile("Resources/Menu/Background.png")) {
        cout << "Erreur chargement Background.png" << endl;
    }
    if (!_inputs.loadFromFile("Resources/Menu/Inputs.png")) {
        cout << "Erreur chargement Inputs.png" << endl;
    }
    if (!_credits.loadFromFile("Resources/Menu/Credits.png")) {
        cout << "Erreur chargement Credits.png" << endl;
    }
    if (!_logo.loadFromFile("Resources/Menu/Logo.png")) {
        cout << "Erreur chargement Logo.png" << endl;
    }
}

sf::RenderWindow *Menu::getWindow() {
    return &_Window;
}

void Menu::action(sf::Keyboard::Key key) {
    if (beforeMenu) {
        beforeMenu = false;
    } else if (inputs) {
        inputs = false;
    } else if (credits) {
        credits = false;
    } else {
        if (key == sf::Keyboard::Up) {
            if (menuIndex > 1) {
                menuIndex--;
            }
        } else if (key == sf::Keyboard::Down) {
            if (menuIndex < 3) {
                menuIndex++;
            }
        } else if (key == sf::Keyboard::Return || key == sf::Keyboard::Space) {
            switch (menuIndex) {
                case 3:
                    credits = true;
                    break;
                case 2:
                    inputs = true;
                    break;
                case 1:
                    _Window.setVisible(false);
                    _music.stop();
                    Game game(800, 640);
                    game.run();
                    _music.play();
                    _Window.setVisible(true);
                    break;
            }
        }
    }
}

Menu::~Menu() {
    delete (_button1);
    delete (_button2);
    delete (_button3);
}
