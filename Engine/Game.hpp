#ifndef GAME_HPP
#define GAME_HPP

#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"


class Game;

#include "Player.hpp"
#include "Map.h"

class Game {
public:
    Game(int width, int height); //Constructeur
    ~Game();

    void run(); //Permet de lancer le jeu
    void nextMap(bool reload);

    Player *_player1;
    Player *_player2;

private:
    void processEvents();

    void update(sf::Time deltaTime);

    void render();

    void drawHUD();

    sf::RenderWindow _Window; //Le jeu gère sa propre fenêtre
    std::string _maps[4];
    int _nbTry;
    sf::Font _font;
    sf::Music _music;
    sf::Texture _inputs;
    sf::Texture _inputsTexture;
    sf::Texture _backgroundText;
    sf::Sprite _background;
};

#endif
