var searchData=
[
  ['_5fdir',['_dir',['../d8/d4b/classObject.html#ade2bfdafc1785da421ec0b47383d48ff',1,'Object']]],
  ['_5fgame',['_game',['../d8/d4b/classObject.html#a821a3820cc812f70a3cdcba028f2be89',1,'Object']]],
  ['_5fi',['_i',['../d8/d4b/classObject.html#a0ddbf697047b0d7e0514d571a6c881e0',1,'Object']]],
  ['_5fisactivated',['_isActivated',['../d1/dd6/classSensor.html#a61d1bb2766f41696ced5d848c2e297cf',1,'Sensor']]],
  ['_5fisreflective',['_isReflective',['../d8/d4b/classObject.html#ad640d83b1612311fdbb2d1b21946634a',1,'Object']]],
  ['_5fj',['_j',['../d8/d4b/classObject.html#acc90a8e7bb7fcd42ae4ebe4460f39489',1,'Object']]],
  ['_5fmovable',['_movable',['../d8/d4b/classObject.html#a7b36fd51749c977cb34ee8663f8eb9b9',1,'Object']]],
  ['_5fobjects',['_objects',['../d9/d68/classGame.html#a2ecb2f2282682b4236e08d1a3695f169',1,'Game']]],
  ['_5fsolid',['_solid',['../d8/d4b/classObject.html#aa0e6c0f2cb3063d38fcfd59c43d20c2b',1,'Object']]],
  ['_5fsprite',['_sprite',['../d8/d4b/classObject.html#ae4a0cff4c80cd9699a032d58f3fd89c9',1,'Object::_sprite()'],['../d2/d4b/classPlayer.html#a41b3ca516dd0f87bfd4615883820843b',1,'Player::_sprite()']]],
  ['_5ftexture',['_texture',['../d8/d4b/classObject.html#a8683c809efd5ad27d09d2ebec58bf99c',1,'Object']]],
  ['_5ftransparent',['_transparent',['../d8/d4b/classObject.html#af66e73264d4fbfebd673ad11d7b1b0a5',1,'Object']]]
];
