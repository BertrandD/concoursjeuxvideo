var searchData=
[
  ['game',['Game',['../d9/d68/classGame.html#a5bc07cf8947858f32637943836cc6f8e',1,'Game']]],
  ['getcolor',['getColor',['../d9/d68/classGame.html#aa2aec935538082fd9d04498e63bf3b40',1,'Game::getColor()'],['../db/d5e/classLazer.html#aac0991099168d7b729c7bb5a35353fee',1,'Lazer::getColor()']]],
  ['getdirection',['getDirection',['../d9/d68/classGame.html#a13d506a4e1a0d1bdf3b223f3c3777d54',1,'Game']]],
  ['getlasersourcedirection',['getLaserSourceDirection',['../de/dfd/classMirror.html#acd222ac44155cab38bae3c3163c44b49',1,'Mirror']]],
  ['getposition',['getPosition',['../d2/d4b/classPlayer.html#a23356f99a9de86d3d47eadb679b332dc',1,'Player']]],
  ['getrotation',['getRotation',['../d8/d4b/classObject.html#adab647a1fc7518893772b41f0b656bc0',1,'Object']]],
  ['getsprite',['getSprite',['../d8/d4b/classObject.html#af99b56ae0ce82a047956aa208734bc02',1,'Object']]],
  ['getwindow',['getWindow',['../d8/ddb/classMenu.html#a547ef660d072a0b5822e5acb49fcabb2',1,'Menu']]]
];
