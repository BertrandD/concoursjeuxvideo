var searchData=
[
  ['player',['Player',['../d2/d4b/classPlayer.html',1,'Player'],['../d2/d4b/classPlayer.html#a8e13448819d0c656a6628938a0526514',1,'Player::Player(std::string name, sf::Color color, Game *game)'],['../d2/d4b/classPlayer.html#a5da5dfe20ad62bcf3e485b7b3c2fac95',1,'Player::Player(std::string name, sf::Color color, Game *game, unsigned int i, unsigned int j)'],['../d2/d4b/classPlayer.html#afa26094b76dedf602d5040bcb78ba3c3',1,'Player::Player(std::string name, sf::Color color, Game *game, unsigned int i, unsigned int j, sf::Keyboard::Key up, sf::Keyboard::Key left, sf::Keyboard::Key down, sf::Keyboard::Key right)']]],
  ['player_2ecpp',['Player.cpp',['../d5/d11/Player_8cpp.html',1,'']]],
  ['player_2ehpp',['Player.hpp',['../d3/d86/Player_8hpp.html',1,'']]],
  ['portal',['Portal',['../d7/d3c/classPortal.html',1,'Portal'],['../d7/d3c/classPortal.html#a56cbb895b12dc7ae8e8288ee4c539cc1',1,'Portal::Portal()']]],
  ['portal_2ecpp',['Portal.cpp',['../da/d5f/Portal_8cpp.html',1,'']]],
  ['portal_2ehpp',['Portal.hpp',['../da/d69/Portal_8hpp.html',1,'']]]
];
