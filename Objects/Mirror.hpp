#ifndef DEF_MIRROR
#define DEF_MIRROR

#include <SFML/Graphics.hpp>
#include "Object.hpp"
#include "Lazer.hpp"

// @Author Leboc Philippe
//   _
//  |/------->
//  |
//  |
//
class Mirror : public Object {
public:
    // Constructeur
    Mirror(int i, int j, Direction dir);

    void drawObject(sf::RenderWindow *window);

    void active(bool active);

    // Methods héritées
    void action(sf::Color color, Direction dir, sf::RenderWindow *window);

    // Methods propres à l'objet Mirror
    Direction getLaserSourceDirection(Direction dir);

    void calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ);

private:
    sf::Sprite getBackSprite();

    sf::Sprite getColorSprite();

    bool _active;
    sf::Color _color;
    sf::Texture _textureColor;
};

#endif