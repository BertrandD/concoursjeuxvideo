#ifndef DEF_CAPTEUR
#define DEF_CAPTEUR

#include "Object.hpp"
#include <SFML/Graphics.hpp>

class Sensor : public Object {
public:
    Sensor(int i, int j, sf::Color color, Direction dir);

    void drawObject(sf::RenderWindow *window);

    void action(sf::Color color);

    bool isActive();

    void disable();

    bool _isActivated;
private:
    sf::Sprite getBackSprite();

    sf::Sprite getColorSprite();

    sf::Color _color;
    sf::Texture _textureColorOn;
    sf::Texture _textureColorOff;
};

#endif
