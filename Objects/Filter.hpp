#ifndef FILTER_H
#define FILTER_H

#include "Object.hpp"

class Filter : public Object {
public:
    Filter(int i, int j, sf::Color color);

    void action(sf::Color color, Direction dir, sf::RenderWindow *window);

    void drawObject(sf::RenderWindow *window);

protected:
private:
    sf::Color _color;

    sf::Color getColor(int code);

    int getRGB(sf::Color color);

    void calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ);
};

#endif
