#ifndef OBJEC_HPP
#define OBJEC_HPP

#include <stdio.h>
#include <stdlib.h>
#include <SFML/Graphics.hpp>

class Object;

#include "../Engine/Player.hpp"
#include "../Engine/Direction.h"

class Object : sf::Transformable {
public:
    Object(int i, int j);

    Object(std::string texturePath, int i, int j);

    virtual void drawObject(sf::RenderWindow *window);

    virtual void action();

    virtual void action(Player *player);

    virtual void action(Direction dir);

    virtual void action(sf::Color color);

    // Utilisée pour transferer le lazer au mirroir
    virtual void action(sf::Color color, Direction dir, sf::RenderWindow *window);

    sf::Sprite getSprite();

    short getRotation();

    bool isSolid();

    virtual bool isTransparent(Direction dir);

    bool canReflect();

    virtual void resetInitial();    // Remet le concentrateur dans son état initial à la fin d'une frame
private:
    void initSprite(std::string texturePath);

protected:
    int _i;
    int _j;
    bool _solid;
    bool _transparent;
    bool _movable;
    Direction _dir;
    sf::Sprite _sprite;
    sf::Texture _texture;
    bool _isReflective;
};

#endif
