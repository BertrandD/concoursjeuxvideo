#ifndef WINDOW_H
#define WINDOW_H

#include <stdio.h>
#include <string>
#include <SFML/Graphics.hpp>
#include "Object.hpp"

class Window : public Object {
public:
    Window(int i, int j, Direction dir);

    bool isTransparent(Direction dir);

protected:
private:
};

#endif
