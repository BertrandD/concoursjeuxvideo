#include "Object.hpp"
#include "../Engine/Map.h"

Object::Object(int i, int j) : _i(i), _j(j), _solid(false), _transparent(true), _movable(false), _dir(Direction::N),
                               _isReflective(false) {
    initSprite("Resources/Textures/empty.png");
}

Object::Object(std::string texturePath, int i, int j) : _i(i), _j(j), _solid(false), _transparent(true),
                                                        _movable(false), _dir(Direction::N), _isReflective(false) {
    initSprite(texturePath);
}

void Object::initSprite(std::string texturePath) {
    if (!_texture.loadFromFile(texturePath)) {
        printf("Erreur lors de la création de l'objet");
    } else {
        _sprite.setTexture(_texture);
        _sprite.setPosition(_i, _j);
    }
}

sf::Sprite Object::getSprite() {
    _sprite.setOrigin(16, 16);
    _sprite.setRotation(getRotation());
    _sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    return _sprite;
}

short Object::getRotation() {
    switch (_dir) {
        case Direction::N:
            return 0;
        case Direction::E:
            return 90;
        case Direction::S:
            return 180;
        case Direction::W:
            return -90;
        default:
            return 0;
    }
}

void Object::drawObject(sf::RenderWindow *window) {
    window->draw(getSprite());
}

bool Object::isSolid() {
    return _solid;
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

bool Object::isTransparent(Direction dir) {
    return _transparent;
}

bool Object::canReflect() {
    return _isReflective;
}

void Object::action() {
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void Object::action(Player *player) {
}

void Object::action(Direction dir)
/*
    Eviter de surcharger cette méthode car c'est elle qui permet le déplacement des objets...
*/
{
    if (_movable) {
        Object *temp;
        switch (dir) {
            case Direction::N:
                if (!Map::getMap()->_objects[_j - 1][_i]->isSolid()) {
                    temp = Map::getMap()->_objects[_j][_i];
                    Map::getMap()->_objects[_j][_i] = Map::getMap()->_objects[_j - 1][_i];
                    Map::getMap()->_objects[_j - 1][_i] = temp;
                    _j -= 1;
                }
                break;
            case Direction::S:
                if (!Map::getMap()->_objects[_j + 1][_i]->isSolid()) {
                    temp = Map::getMap()->_objects[_j][_i];
                    Map::getMap()->_objects[_j][_i] = Map::getMap()->_objects[_j + 1][_i];
                    Map::getMap()->_objects[_j + 1][_i] = temp;
                    _j += 1;
                }
                break;
            case Direction::W:
                if (!Map::getMap()->_objects[_j][_i - 1]->isSolid()) {
                    temp = Map::getMap()->_objects[_j][_i];
                    Map::getMap()->_objects[_j][_i] = Map::getMap()->_objects[_j][_i - 1];
                    Map::getMap()->_objects[_j][_i - 1] = temp;
                    _i -= 1;
                }
                break;
            case Direction::E:
                if (!Map::getMap()->_objects[_j][_i + 1]->isSolid()) {
                    temp = Map::getMap()->_objects[_j][_i];
                    Map::getMap()->_objects[_j][_i] = Map::getMap()->_objects[_j][_i + 1];
                    Map::getMap()->_objects[_j][_i + 1] = temp;
                    _i += 1;
                }
                break;
            default:
                break;
        }
    }
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void Object::action(sf::Color color) {
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void Object::action(sf::Color color, Direction dir, sf::RenderWindow *window) {
}

void Object::resetInitial() {
}
