#ifndef PORTAL_HPP
#define PORTAL_HPP

#include "Object.hpp"

class Portal : Object {
public:
    Portal(int i, int j, int output);

    void action(Player *player);

private:
    int _output;
};

#endif