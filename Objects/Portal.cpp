#include "Portal.hpp"

Portal::Portal(int i, int j, int output) : Object("Resources/Textures/Portal.png", i, j) {
    _solid = false;
    _transparent = true;
    _movable = false;
    _dir = Direction::N;
    _output = output;
}

void Portal::action(Player *player) {
    int i = _output % 25;
    int j = _output / 25;
    player->setPosition(i, j);
}