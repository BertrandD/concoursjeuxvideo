#include "EndMap.hpp"


EndMap::EndMap(int i, int j, std::string texturePath, std::string texturePathOpen) : Object(texturePath, i, j) {
    _solid = true;
    _transparent = false;
    _movable = false;
    _dir = Direction::N;
    if (!_textureOpen.loadFromFile(texturePathOpen)) {
        printf("Erreur lors de la création de l'objet");
    }
}

#pragma GCC diagnostic ignored "-Wunused-parameter"

void EndMap::action(Player *player) {
    _reached = true;
}

void EndMap::open() {
    _sprite.setTexture(_textureOpen);
    _solid = false;
    _transparent = true;
}

void EndMap::close() {
    _sprite.setTexture(_texture);
    _solid = true;
    _transparent = false;
}

bool EndMap::isReached() {
    return _reached;
}
