#ifndef LAZER_H
#define LAZER_H

#include <SFML/Graphics.hpp>
#include "Object.hpp"

class Lazer : public Object {
public:
    Lazer(int i, int j, Direction dir);

    Lazer(int i, int j, sf::Color color, Direction dir);

    void drawObject(sf::RenderWindow *window);

    sf::Color getColor();

    void active(bool active);

    void resetInitial();

protected:
private:
    bool _active;

    sf::Sprite getBackSprite();

    sf::Sprite getColorSprite();

    sf::Color _color;
    sf::Texture _textureColor;

    void calculOffsetsAndPositions(int *offsetX, int *offsetY, int *curI, int *curJ);
};

#endif // LAZER_H
