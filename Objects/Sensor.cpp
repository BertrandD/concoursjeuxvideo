#include "Sensor.hpp"
#include <iostream>

Sensor::Sensor(int i, int j, sf::Color color, Direction dir) : Object("Resources/Textures/wall.png", i, j) {
    _isActivated = false;
    _color = color;
    _solid = true;
    _transparent = false;
    _dir = dir;

    std::string path = "Resources/Textures/Sensor_on.png";
    if (!_textureColorOn.loadFromFile(path)) {
        printf("Erreur lors de la création de l'objet");
    }

    path = "Resources/Textures/Sensor_off.png";
    if (!_textureColorOff.loadFromFile(path)) {
        printf("Erreur lors de la création de l'objet");
    }
}

void Sensor::drawObject(sf::RenderWindow *window) {
    window->draw(getBackSprite());
    window->draw(getColorSprite());
}

void Sensor::action(sf::Color color) {
    if (color == _color) {
        _isActivated = true;
    }
}

bool Sensor::isActive() {
    return _isActivated;
}

sf::Sprite Sensor::getBackSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_texture);
    sprite.rotate(getRotation());
    return sprite;
}

sf::Sprite Sensor::getColorSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    if (_isActivated)
        sprite.setTexture(_textureColorOn);
    else
        sprite.setTexture(_textureColorOff);
    sprite.setColor(_color);
    sprite.rotate(getRotation());
    return sprite;
}

void Sensor::disable() {
    _isActivated = false;
}
