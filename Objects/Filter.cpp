#include "Filter.hpp"
#include "../Engine/Map.h"

Filter::Filter(int i, int j, sf::Color color) : Object("Resources/Textures/Filter.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = true;
    _isReflective = true;
    _color = color;

}

void Filter::action(sf::Color color, Direction dir, sf::RenderWindow *window) {
    color = getColor(getRGB(color) & getRGB(_color));
    if (color != sf::Color::Black) {
        int curI = _i;
        int curJ = _j;
        sf::RectangleShape rectangle;
        if (dir == Direction::W || dir == Direction::E) {
            rectangle.setSize(sf::Vector2f(32, 4));
        } else {
            rectangle.setSize(sf::Vector2f(4, 32));
        }
        rectangle.setFillColor(color);
        int offsetX = 0;
        int offsetY = 0;
        calculOffsetsAndPositions(dir, &offsetX, &offsetY, &curI, &curJ);
        while (Map::getMap()->_objects[curJ][curI]->isTransparent(dir)) {
            rectangle.setPosition(curI * 32 + offsetX, curJ * 32 + offsetY);
            window->draw(rectangle);
            calculOffsetsAndPositions(dir, &offsetX, &offsetY, &curI, &curJ);
        }

        if (Map::getMap()->_objects[curJ][curI]->canReflect()) {
            // _dir est la direction actuelle du laser.
            Map::getMap()->_objects[curJ][curI]->action(color, dir, window);
        } else {
            Map::getMap()->_objects[curJ][curI]->action(color);
        }
        Map::getMap()->_objects[curJ][curI]->drawObject(window);
    }
}

void Filter::drawObject(sf::RenderWindow *window) {
    sf::Sprite sp = getSprite();
    sp.setColor(_color);
    window->draw(sp);
}

sf::Color Filter::getColor(int code) {
    sf::Color color;
    switch (code) {
        case 0:
            color = sf::Color::Black;
            break;
        case 1:
            color = sf::Color::Blue;
            break;
        case 2:
            color = sf::Color::Green;
            break;
        case 3:
            color = sf::Color::Cyan;
            break;
        case 4:
            color = sf::Color::Red;
            break;
        case 5:
            color = sf::Color::Magenta;
            break;
        case 6:
            color = sf::Color::Yellow;
            break;
        case 7:
            color = sf::Color::White;
            break;
        default:
            color = sf::Color::Black;
    }
    return color;
}

int Filter::getRGB(sf::Color color) {
    int code;
    if (color == sf::Color::Black)
        code = 0;
    else if (color == sf::Color::Blue)
        code = 1;
    else if (color == sf::Color::Green)
        code = 2;
    else if (color == sf::Color::Cyan)
        code = 3;
    else if (color == sf::Color::Red)
        code = 4;
    else if (color == sf::Color::Magenta)
        code = 5;
    else if (color == sf::Color::Yellow)
        code = 6;
    else if (color == sf::Color::White)
        code = 7;
    else
        code = 0;
    return code;
}

void Filter::calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ) {
    switch (dir) {
        case Direction::N:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
        case Direction::E:
            *offsetX = 0;
            *offsetY = 14;
            *curI += 1;
            break;
        case Direction::S:
            *offsetX = 14;
            *offsetY = 0;
            *curJ += 1;
            break;
        case Direction::W:
            *offsetX = 0;
            *offsetY = 14;
            *curI -= 1;
            break;
        default:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
    }
}
