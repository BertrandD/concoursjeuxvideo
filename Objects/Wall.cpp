#include "Wall.hpp"

Wall::Wall(int i, int j) : Object("Resources/Textures/wall.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = false;
    _dir = Direction::N;
}