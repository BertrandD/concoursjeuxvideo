#include "Concentrator.hpp"
#include "../Engine/Map.h"

Concentrator::Concentrator(int i, int j, Direction dir) : Object("Resources/Textures/Concentrator.png", i, j) {
    _color = sf::Color::Black;
    _E = sf::Color::Black;
    _S = sf::Color::Black;
    _W = sf::Color::Black;
    _dir = dir;
    _solid = true;
    _transparent = false;
    _movable = false;
    _isReflective = true;
    if (!_textureColor.loadFromFile("Resources/Textures/Lazer_color.png"))
        printf("Erreur lors de la création de l'objet");
    if (!_textureSide.loadFromFile("Resources/Textures/Concentrator_R.png"))
        printf("Erreur lors de la création de l'objet");
    if (!_textureColor.loadFromFile("Resources/Textures/Lazer_color.png"))
        printf("Erreur lors de la création de l'objet");

}

sf::Sprite Concentrator::getBackSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_texture);
    sprite.rotate(getRotation());
    return sprite;
}

sf::Sprite Concentrator::getColorSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_textureColor);
    sprite.setColor(_color);
    sprite.rotate(getRotation());
    return sprite;
}

// Définit pour la position initiale du concentrator (Nord)
sf::Sprite Concentrator::getSideSprite(Direction dir) {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_textureSide);
    if (dir == Direction::E) {
        sprite.setColor(_E);
        sprite.rotate(getRotation());
    } else if (dir == Direction::W) {
        sprite.setColor(_W);
        sprite.rotate(getRotation() + 180.0);
    } else {
        sprite.setColor(_S);
        sprite.rotate(getRotation() + 90.0);
    }

    return sprite;
}

void Concentrator::drawObject(sf::RenderWindow *window) {
    window->draw(getBackSprite());
    window->draw(getColorSprite());
    window->draw(getSideSprite(Direction::E));
    window->draw(getSideSprite(Direction::S));
    window->draw(getSideSprite(Direction::W));

}

void Concentrator::action(sf::Color color, Direction dir, sf::RenderWindow *window) {
// retourne la direction de réflection.
    switch (_dir) {
        case Direction::N :
            if (dir == Direction::W)
                _E = color;
            else if (dir == Direction::E)
                _W = color;
            else if (dir == Direction::N)
                _S = color;
            break;
        case Direction::S :
            if (dir == Direction::S)
                _S = color;
            else if (dir == Direction::E)
                _E = color;
            else if (dir == Direction::W)
                _W = color;
            break;
        case Direction::W :
            if (dir == Direction::N)
                _W = color;
            else if (dir == Direction::S)
                _E = color;
            else if (dir == Direction::W)
                _S = color;
            break;
        case Direction::E :
            if (dir == Direction::S)
                _W = color;
            else if (dir == Direction::E)
                _S = color;
            else if (dir == Direction::N)
                _E = color;
            break;
        default:
            if (dir == Direction::W)
                _E = color;
            else if (dir == Direction::E)
                _W = color;
            else if (dir == Direction::N)
                _S = color;
            break;
    }
    _color = (getColor((getRGB(_E) | getRGB(_S)) | getRGB(_W)));
    if (_color != sf::Color::Black) {
        int curI = _i;
        int curJ = _j;
        sf::RectangleShape rectangle;
        if (_dir == Direction::W || _dir == Direction::E) {
            rectangle.setSize(sf::Vector2f(32, 4));
        } else {
            rectangle.setSize(sf::Vector2f(4, 32));
        }
        rectangle.setFillColor(_color);
        int offsetX = 0;
        int offsetY = 0;
        calculOffsetsAndPositions(_dir, &offsetX, &offsetY, &curI, &curJ);
        while (Map::getMap()->_objects[curJ][curI]->isTransparent(_dir)) {
            rectangle.setPosition(curI * 32 + offsetX, curJ * 32 + offsetY);
            window->draw(rectangle);
            calculOffsetsAndPositions(_dir, &offsetX, &offsetY, &curI, &curJ);
        }

        if (Map::getMap()->_objects[curJ][curI]->canReflect()) {
            // _dir est la direction actuelle du laser.
            Map::getMap()->_objects[curJ][curI]->action(_color, _dir, window);
        } else {
            Map::getMap()->_objects[curJ][curI]->action(_color);
        }
        Map::getMap()->_objects[curJ][curI]->drawObject(window);
    }
}

// Retourne un code type RGB selon color
int Concentrator::getRGB(sf::Color color) {
    int code;
    if (color == sf::Color::Black)
        code = 0;
    else if (color == sf::Color::Blue)
        code = 1;
    else if (color == sf::Color::Green)
        code = 2;
    else if (color == sf::Color::Cyan)
        code = 3;
    else if (color == sf::Color::Red)
        code = 4;
    else if (color == sf::Color::Magenta)
        code = 5;
    else if (color == sf::Color::Yellow)
        code = 6;
    else if (color == sf::Color::White)
        code = 7;
    else
        code = 0;
    return code;
}

// Retourne la couleur correspondant au code RGB
sf::Color Concentrator::getColor(int code) {
    sf::Color color;
    switch (code) {
        case 0:
            color = sf::Color::Black;
            break;
        case 1:
            color = sf::Color::Blue;
            break;
        case 2:
            color = sf::Color::Green;
            break;
        case 3:
            color = sf::Color::Cyan;
            break;
        case 4:
            color = sf::Color::Red;
            break;
        case 5:
            color = sf::Color::Magenta;
            break;
        case 6:
            color = sf::Color::Yellow;
            break;
        case 7:
            color = sf::Color::White;
            break;
        default:
            color = sf::Color::Black;
    }
    return color;
}

void Concentrator::calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ) {
    switch (dir) {
        case Direction::N:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
        case Direction::E:
            *offsetX = 0;
            *offsetY = 14;
            *curI += 1;
            break;
        case Direction::S:
            *offsetX = 14;
            *offsetY = 0;
            *curJ += 1;
            break;
        case Direction::W:
            *offsetX = 0;
            *offsetY = 14;
            *curI -= 1;
            break;
        default:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
    }
}

void Concentrator::resetInitial() {
    _color = sf::Color::Black;
    _E = sf::Color::Black;
    _S = sf::Color::Black;
    _W = sf::Color::Black;
}
