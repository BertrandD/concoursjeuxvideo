#include "Mirror.hpp"
#include "../Engine/Map.h"

// @Author Leboc Philippe
//   _
// 	|/------->
//  |
//  |

Mirror::Mirror(int i, int j, Direction dir) : Object("Resources/Textures/Mirror.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = true;
    _isReflective = true;
    _dir = dir;
    if (!_textureColor.loadFromFile("Resources/Textures/Mirror_Color.png")) {
        printf("Erreur lors de la création de l'objet");
    }
    _active = false;
}

sf::Sprite Mirror::getBackSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_texture);
    sprite.rotate(getRotation());
    return sprite;
}

sf::Sprite Mirror::getColorSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_textureColor);
    sprite.setColor(_color);
    sprite.rotate(getRotation());
    return sprite;
}

void Mirror::drawObject(sf::RenderWindow *window) {
    window->draw(getBackSprite());
    if (_active)
        window->draw(getColorSprite());
    _active = false;
}

void Mirror::active(bool active) {
    _active = active;
}

void Mirror::action(sf::Color color, Direction dir, sf::RenderWindow *window) {
    // retourne la direction de réflection
    Direction reflectDirection = getLaserSourceDirection(dir);
    if (reflectDirection != Direction::UNDEFINED) {
        _active = true;
        // Récupère la position actuelle du mirroir
        int curI = _i;
        int curJ = _j;

        // Dessine la ligne sur la direction.
        sf::RectangleShape rectangle;
        // Repositionne correctement le laser
        if (reflectDirection == Direction::W || reflectDirection == Direction::E) {
            rectangle.setSize(sf::Vector2f(32, 4));
        } else {
            rectangle.setSize(sf::Vector2f(4, 32));
        }
        rectangle.setFillColor(color);
        _color = color;

        int offsetX = 0;
        int offsetY = 0;

        calculOffsetsAndPositions(reflectDirection, &offsetX, &offsetY, &curI, &curJ);
        while (Map::getMap()->_objects[curJ][curI]->isTransparent(reflectDirection)) {
            rectangle.setPosition(curI * 32 + offsetX, curJ * 32 + offsetY);
            window->draw(rectangle);
            calculOffsetsAndPositions(reflectDirection, &offsetX, &offsetY, &curI, &curJ);
        }

        if (Map::getMap()->_objects[curJ][curI]->canReflect()) {
            // _dir est la direction actuelle du laser.
            // Mirror s'occupera d'inverser pour savoir d'où vient le laser.
            Map::getMap()->_objects[curJ][curI]->action(color, reflectDirection, window);
        } else {
            Map::getMap()->_objects[curJ][curI]->action(color);
        }
        Map::getMap()->_objects[curJ][curI]->drawObject(window);
    }
}

// Renvoie la bonne direction pour renvoyer le laser.
// C'est moche =/
Direction Mirror::getLaserSourceDirection(Direction LaserDir) {
    switch (_dir) {
        case Direction::N:
            switch (LaserDir) {
                case Direction::N:
                    LaserDir = Direction::W;
                    break;
                case Direction::W:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::S:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::E:
                    LaserDir = Direction::S;
                    break;
                default:
                    break;
            }
            break;
        case Direction::S:
            switch (LaserDir) {
                case Direction::N:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::W:
                    LaserDir = Direction::N;
                    break;
                case Direction::S:
                    LaserDir = Direction::E;
                    break;
                case Direction::E:
                    LaserDir = Direction::UNDEFINED;
                    break;
                default:
                    break;
            }
            break;
        case Direction::E:
            switch (LaserDir) {
                case Direction::N:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::W:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::S:
                    LaserDir = Direction::W;
                    break;
                case Direction::E:
                    LaserDir = Direction::N;
                    break;
                default:
                    break;
            }
            break;
        case Direction::W:
            switch (LaserDir) {
                case Direction::N:
                    LaserDir = Direction::E;
                    break;
                case Direction::W:
                    LaserDir = Direction::S;
                    break;
                case Direction::S:
                    LaserDir = Direction::UNDEFINED;
                    break;
                case Direction::E:
                    LaserDir = Direction::UNDEFINED;
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return LaserDir;
}

void Mirror::calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ) {
    switch (dir) {
        case Direction::N:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
        case Direction::E:
            *offsetX = 0;
            *offsetY = 14;
            *curI += 1;
            break;
        case Direction::S:
            *offsetX = 14;
            *offsetY = 0;
            *curJ += 1;
            break;
        case Direction::W:
            *offsetX = 0;
            *offsetY = 14;
            *curI -= 1;
            break;
        default:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
    }
}