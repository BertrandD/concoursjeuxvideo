#ifndef CONCENTRATOR_H
#define CONCENTRATOR_H

#include "Object.hpp"

class Concentrator : public Object {
public:
    Concentrator(int i, int j, Direction dir);

    void action(sf::Color color, Direction dir, sf::RenderWindow *window);

    void drawObject(sf::RenderWindow *window);

    void resetInitial();    // Remet le concentrateur dans son �tat initial � la fin d'une frame
protected:
private:
    sf::Texture _textureColor;
    sf::Texture _textureSide;
    sf::Color _color;
    sf::Color _S;
    sf::Color _W;
    sf::Color _E;

    sf::Sprite getSideSprite(Direction dir);

    sf::Sprite getColorSprite();

    sf::Sprite getBackSprite();

    sf::Color getColor(int code);

    int getRGB(sf::Color color);

    void calculOffsetsAndPositions(Direction dir, int *offsetX, int *offsetY, int *curI, int *curJ);
};

#endif // CONCENTRATOR_H
