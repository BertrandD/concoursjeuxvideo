#ifndef WALL_H
#define WALL_H

#include <stdio.h>
#include <string>
#include <SFML/Graphics.hpp>
#include "Object.hpp"

class Wall : public Object {
public:
    Wall(int i, int j);

protected:
private:
};

#endif // WALL_H
