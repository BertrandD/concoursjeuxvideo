#ifndef ENDMAP_HPP
#define ENDMAP_HPP

#include "Object.hpp"

class EndMap : public Object {
public:
    EndMap(int i, int j, std::string texturePath, std::string texturePathOpen);

    void action(Player *player);

    void open();

    void close();

    bool isReached();

private:
    sf::Texture _textureOpen;
    bool _reached;
};

#endif
