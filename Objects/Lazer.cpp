#include "Lazer.hpp"
#include "../Engine/Map.h"

Lazer::Lazer(int i, int j, Direction dir) : Object("Resources/Textures/Lazer.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = true;
    _dir = dir;
    if (!_textureColor.loadFromFile("Resources/Textures/Lazer_color.png")) {
        printf("Erreur lors de la création de l'objet");
    }
}

Lazer::Lazer(int i, int j, sf::Color color, Direction dir) : Object("Resources/Textures/Lazer.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = true;
    _dir = dir;
    _color = color;
    if (!_textureColor.loadFromFile("Resources/Textures/Lazer_color.png")) {
        printf("Erreur lors de la création de l'objet");
    }
}

sf::Sprite Lazer::getBackSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_texture);
    sprite.rotate(getRotation());
    return sprite;
}

sf::Sprite Lazer::getColorSprite() {
    sf::Sprite sprite;
    sprite.setOrigin(16, 16);
    sprite.setPosition(_i * 32 + 16, _j * 32 + 16);
    sprite.setTexture(_textureColor);
    sprite.setColor(_color);
    sprite.rotate(getRotation());
    return sprite;
}

void Lazer::drawObject(sf::RenderWindow *window) {
    // Spawn du générateur de laser.
    window->draw(getBackSprite());
    window->draw(getColorSprite());

    if (!_active) {
        active(true);
        // Dessine la ligne sur la direction.
        sf::RectangleShape rectangle(sf::Vector2f(4, 32));

        // dessine le lazer
        int curI = _i;
        int curJ = _j;
        rectangle.setFillColor(_color);
        rectangle.rotate(getRotation());

        int offsetX = 0;
        int offsetY = 0;

        calculOffsetsAndPositions(&offsetX, &offsetY, &curI, &curJ);
        while (Map::getMap()->_objects[curJ][curI]->isTransparent(_dir)) {
            rectangle.setPosition(curI * 32 + offsetX, curJ * 32 + offsetY);
            window->draw(rectangle);
            calculOffsetsAndPositions(&offsetX, &offsetY, &curI, &curJ);
        }

        if (Map::getMap()->_objects[curJ][curI]->canReflect()) {
            // _dir est la direction actuelle du laser.
            // Mirror s'occupera d'inverser pour savoir d'où vient le laser.
            Map::getMap()->_objects[curJ][curI]->action(_color, _dir, window);
        } else {
            Map::getMap()->_objects[curJ][curI]->action(_color);
        }
        Map::getMap()->_objects[curJ][curI]->drawObject(window);
    }

}

void Lazer::calculOffsetsAndPositions(int *offsetX, int *offsetY, int *curI, int *curJ)
/*
    Pour éviter d'écrire deux fois le même switch....
    @Philippe: Pas con mon gars ! :)
    @Bertrand: Merci ^^
*/
{
    switch (_dir) {
        case Direction::N:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
        case Direction::E:
            *offsetX = 32;
            *offsetY = 14;
            *curI += 1;
            break;
        case Direction::S:
            *offsetX = 18;
            *offsetY = 32;
            *curJ += 1;
            break;
        case Direction::W:
            *offsetX = 0;
            *offsetY = 18;
            *curI -= 1;
            break;
        default:
            *offsetX = 14;
            *offsetY = 0;
            *curJ -= 1;
            break;
    }
}

sf::Color Lazer::getColor() {
    return _color;
}

void Lazer::active(bool active) {
    _active = active;
}

void Lazer::resetInitial() {
    active(false);
}
