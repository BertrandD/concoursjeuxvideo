#include "Window.hpp"


Window::Window(int i, int j, Direction dir) : Object("Resources/Textures/Window.png", i, j) {
    _solid = true;
    _transparent = false;
    _movable = false;
    _dir = dir;
}

bool Window::isTransparent(Direction dir) {
    return ((_dir == Direction::N || _dir == Direction::S) && (dir == Direction::N || dir == Direction::S)) ||
           ((_dir == Direction::E || _dir == Direction::W) && (dir == Direction::E || dir == Direction::W));
}

